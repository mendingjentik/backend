from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from .rdf_helper import *
from .serializers import MovieSerializer

# Create your views here.
class MoviesView(APIView):
    def get(self, request, format=None):
        get_by_year = request.query_params.get('year')
        get_by_title = request.query_params.get('title')
        get_by_actor = request.query_params.get('actor')
        get_by_director = request.query_params.get('director')
        get_by_genre = request.query_params.get('genre')

        if get_by_year != None and get_by_year != "":
            if get_by_year.isdigit() == False :
                return Response(data={'response':'Year should be a number'}, headers= {"Access-Control-Allow-Origin":"*"},status=400)

        querying_result = query_constructor(get_by_year, get_by_title, get_by_actor, get_by_director, get_by_genre)
        if querying_result == []:
            if get_by_title != None:
                querying_result_dbpedia = dbpedia_query(get_by_title)
                results = MovieSerializer(querying_result_dbpedia, many=True).data
                return Response(results, headers = {"Access-Control-Allow-Origin":"*"},status=200)
        results = MovieSerializer(querying_result, many=True).data
        return Response(results, headers = {"Access-Control-Allow-Origin":"*"},status=200)
