from rdflib import Graph, Literal, RDF, URIRef, Namespace #basic RDF handling
from rdflib.namespace import FOAF , XSD #most common namespaces
from SPARQLWrapper import SPARQLWrapper, JSON
import urllib.parse #for parsing strings to URI's
import ast
import datetime

g = Graph()
g.parse("indonesian_movies_mj.ttl")

BASE_QUERY = """
        PREFIX mj: <http://mendingjentik.org/data/>
        PREFIX dbr: <https://dbpedia.org/resource/>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

        SELECT ?title ?description ?directors ?genre ?languages ?runtime ?users_rating ?votes ?year ((GROUP_CONCAT(?actor;SEPARATOR=", ")) AS ?actors)
        WHERE {
            ?s mj:title ?title .
            ?s mj:description ?description .
            ?s mj:directors ?directors .
            ?s mj:genre ?genre .
            ?s mj:languages ?languages .
            ?s mj:runtime ?runtime .
            ?s mj:users_rating ?users_rating .
            ?s mj:votes ?votes .
            ?s mj:year ?year .
            ?s mj:actor ?actor .
        """

END_QUERY = """
        }
        GROUP BY ?s 
        """

def query_constructor(year, title, actor, director, genre):
    query = BASE_QUERY
    if year != None:
        year_query = """FILTER(?year = "%i"^^xsd:gYear) .""" % int(year)
        query += year_query

    if title != None:
        title_query = """FILTER contains (lcase(str(?title)), "%s") .""" % title
        query += title_query

    if actor != None:
        actor_query = """FILTER contains (lcase(str(?actor)), "%s") .""" % actor
        query += actor_query

    if director != None:
        director_query = """FILTER contains (lcase(str(?directors)), "%s") .""" % director
        query += director_query

    if genre != None:
        genre_query = """FILTER contains (lcase(str(?genre)), "%s") .""" % genre
        query += genre_query

    query += END_QUERY
    return query_executor(query)

def query_executor(query):
    query_result = g.query(query)
    final_result = []
    
    for result in query_result:
        result = result.asdict()
        row = []
        for key in result:
            row.append(result[key].toPython())
        row = tuple(row)
        final_result.append(row)
    
    final_result = list(set(final_result))
    final_final_result = []
    
    for item in final_result:
      value = list(item)
      key = ["title", "description", "directors", "genre", "languages", "runtime", "users_rating", "votes", "year", "actor"]
      if type(value[8]) == datetime.date:
        value[8] = value[8].strftime("%Y")
      final_final_result.append(dict(zip(key, value)))
    return(final_final_result)

def dbpedia_query(title):
    query_sparql = """
    PREFIX dbo:  <http://dbpedia.org/ontology/>
    PREFIX dbp: <http://dbpedia.org/property/>
    SELECT DISTINCT  ?s ?name ?abstract ?director ?language ?runtime ((GROUP_CONCAT(?star;SEPARATOR=", ")) AS ?stars)
    WHERE{ 
       ?s rdf:type  dbo:Film.
       ?s rdfs:label ?name.
       ?s dbo:abstract ?abstract.
       ?s dbo:director ?director.
       ?s dbp:language ?language.
       ?s dbp:starring ?star.
       ?s dbp:runtime ?runtime
       FILTER REGEX (str(?name), "%s", "i") .
       FILTER (lang(?name) = 'en') .
       FILTER (lang(?abstract) = 'en') .
    }
    """ % title

    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(query_sparql)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    finale = []

    for result in results["results"]["bindings"]:
        row = {}
        row['actor'] = result['stars']['value']
        row['description'] = result['abstract']['value']
        row['directors'] = result['director']['value']
        row['genre'] = 'nan'
        row['languages'] = result['language']['value']
        row['runtime'] = str(int(float(result['runtime']['value'])//60)) + " min"
        row['title'] = result['name']['value']
        row['users_rating'] = 'nan'
        row['votes'] = 'nan'
        row['year'] = 'nan'
        finale.append(row)

    return finale