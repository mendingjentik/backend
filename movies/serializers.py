from rest_framework import serializers

class MovieSerializer(serializers.Serializer):
   title = serializers.CharField()
   description = serializers.CharField()
   directors = serializers.CharField()
   genre = serializers.CharField()
   languages = serializers.CharField()
   runtime = serializers.CharField()
   users_rating = serializers.CharField()
   votes = serializers.CharField()
   year = serializers.CharField()
   actor = serializers.CharField()
   